<?php
  $page = get_post(10);
?>
    <div class="container-fluid">
      <div id="target_<?php echo $page->ID?>" class="row entorno gray-back">
        <!-- <img class="entorno-img" src="/img/ENTORNO.png" alt=""> -->
        <div class="col-xs-12 col-sm-12 col-md-4">
          <h1 class="purple"><?php echo get_field('titulo',$page->ID);?></h1>
          <h2 class="purple"><?php echo get_field('subtitulo',$page->ID);?></h2>
          <div class="leyenda">
            <ul>
              <li onClick="bounceMarker('stop'); bounceMarker('colegios')">
                <div class="icon-leyenda" onClick="$('.icon-leyenda').removeClass('active'); $(this).addClass('active');"><img class="alignnone size-full wp-image-211" src="<?php echo get_template_directory_uri()?>/img/iconos-ubicacion/colegios.png" alt="" width="27" height="27" /></div><span>Colegios</span>
              </li>

              <li onClick="bounceMarker('stop'); bounceMarker('supermercados')">
                <div class="icon-leyenda"  onClick="$('.icon-leyenda').removeClass('active'); $(this).addClass('active');" ><img class="alignnone size-full wp-image-210" src="<?php echo get_template_directory_uri()?>/img/iconos-ubicacion/supermercado.png" alt="" width="27" height="25" /></div><span>Supermercados</span>
              </li>

              <li>
                <div  onClick="$('.icon-leyenda').removeClass('active'); $(this).addClass('active'); bounceMarker('stop'); bounceMarker('universidades')" class="icon-leyenda"><img class="alignnone size-full wp-image-211" src="<?php echo get_template_directory_uri()?>/img/iconos-ubicacion/universidad.png" alt="" width="27" height="27" /></div><span>Universidades</span>
              </li>

              <li>
                <div  onClick="$('.icon-leyenda').removeClass('active'); $(this).addClass('active'); bounceMarker('stop'); bounceMarker('bancos')" class="icon-leyenda"><img class="alignnone size-full wp-image-212" src="<?php echo get_template_directory_uri()?>/img/iconos-ubicacion/bancos.png" alt="" width="26" height="26" /></div><span>Bancos</span>
              </li>

              <li>
                <div  onClick="$('.icon-leyenda').removeClass('active'); $(this).addClass('active'); bounceMarker('stop'); bounceMarker('restaurantes')" class="icon-leyenda"><img class="alignnone size-full wp-image-213" src="<?php echo get_template_directory_uri()?>/img/iconos-ubicacion/restaurant.png" alt="" width="31" height="27" /></div><span>Restaurantes</span>
              </li>

              <li>
                <div  onClick="$('.icon-leyenda').removeClass('active'); $(this).addClass('active'); bounceMarker('stop'); bounceMarker('clinicas')" class="icon-leyenda"><img class="alignnone size-full wp-image-214" src="<?php echo get_template_directory_uri()?>/img/iconos-ubicacion/centrosalud-g.png" alt="" width="25" height="21" /></div><span>Clínicas</span>
              </li>

              <li>
                <div  onClick="$('.icon-leyenda').removeClass('active'); $(this).addClass('active'); bounceMarker('stop'); bounceMarker('iglesias')" class="icon-leyenda"><img class="alignnone size-full wp-image-214" src="<?php echo get_template_directory_uri()?>/img/iconos-ubicacion/iglesia.png" alt="" width="25" height="21" /></div><span>Iglesias</span>
              </li>

            </ul>

          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8">
          <div class="map">
             <div id="map"></div>
          </div>
          <div class="icon_google hidden-xs hidden-sm text-right" onClick="window.open('https://www.google.cl/maps/place/Estoril+820,+Las+Condes,+Regi%C3%B3n+Metropolitana/@-33.3889237,-70.5298652,17z/data=!3m1!4b1!4m5!3m4!1s0x9662cea9eeeb96ff:0xaa712863fa956268!8m2!3d-33.3889237!4d-70.5276765?hl=es')"><img src="<?php echo get_template_directory_uri()?>/img/icon_maps.png"></div>
          <div class="icon_waze hidden-lg hidden-md text-right" onClick="window.open('https://embed.waze.com/iframe?zoom=14&lat=-33.3889237&lon=-70.5298652&desc=1&group=1&pin=1')"><img src="<?php echo get_template_directory_uri()?>/img/icon_waze.png"></div>
        </div>
      </div>
    </div>
