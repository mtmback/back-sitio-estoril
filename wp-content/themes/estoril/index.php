<?php
/*
Template Name: Home;
*/
?>

<?php get_header(); ?>

<div id="main">

    <?php include('module_home.php'); ?>

    <!-- PROYECTO -->
    <?php include('module_proyecto.php'); ?>

    <!-- DPTOS JARDÍN -->
    <?php include('module_p_jardin.php'); ?>

    <!-- DPTOS QUINCHOS -->
    <?php include('module_p_quincho.php'); ?>

    <?php //include('module_vista_modelo.php'); ?>
    <?php include('module_cotizacion.php'); ?>


    <!-- ENTORNO MAPA -->
    <?php include('module_entorno.php'); ?>

    <!-- CONTACTO -->
    <?php include('module_contacto.php'); ?>

    

</div>
<?php get_footer(); ?>
