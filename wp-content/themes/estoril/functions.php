<?php
function estoril_wpsMimeTypes( $mimes ){
$mimes['svg'] = 'image/svg+xml';
return $mimes;
}
add_filter( 'upload_mimes', 'estoril_wpsMimeTypes' );

function estoril_registerMenus() {
	register_nav_menus(
		array(
		'header-menu' => __( 'Header Menu' ),
		)
	);
}
add_action( 'init', 'estoril_registerMenus' );

function estoril_wpGetMenuArray($current_menu) {

	$array_menu = wp_get_nav_menu_items($current_menu);
	$menu = array();
	foreach ($array_menu as $m) {
		//var_dump($m);
		if (empty($m->menu_item_parent)) {
			$menu[$m->ID] = array();
			$menu[$m->ID]['ID'] = $m->ID;
			$menu[$m->ID]['object_id'] = $m->object_id;
			$menu[$m->ID]['title'] = $m->title;
			$menu[$m->ID]['url'] = $m->url;
			$menu[$m->ID]['children'] = array();
		}
	}
	$submenu = array();
	foreach ($array_menu as $m) {
		if ($m->menu_item_parent) {
			$submenu[$m->ID] = array();
			$submenu[$m->ID]['ID'] = $m->ID;
			$submenu[$m->ID]['object_id'] = $m->object_id;
			$submenu[$m->ID]['title'] = $m->title;
			$submenu[$m->ID]['url'] = $m->url;
			$menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
		}
	}
	return $menu;

}

/**
 * Register a custom menu page.
 */
function wpdocs_register_exportar(){
    add_menu_page( 
        __( 'Exportar', 'textdomain' ),
        'Exportar Excel',
        'manage_options',
        'custompage',
        'exportar',
        'dashicons-list-view',
        6
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_exportar' );
 
/**
 * Display a custom menu page
 */
function exportar(){
    //esc_html_e( 'Admin Page Test', 'textdomain' );

	?>
		<h1>Exportar a Excel</h1>
		<hr>
		<p>Por seguridad se pide una clave para exportar los datos de tus clientes. Pero en este caso ya se encuentra tipeada, sólo requieres hacer click en el boton "Proceder"</p>
		<p>( La clave se configura en la pagina de exportación )</p>
		<form method="POST" action="<?php echo get_permalink(354); ?>">
			<input type="password" name="clave" value="<?php echo get_field('clave',354);?>">
			<hr>
			<input type="submit" name="" value="Proceder">
		</form>
	<?php
}

function custom_theme_setup() {
	add_theme_support( 'advanced-image-compression' );
}
add_action( 'after_setup_theme', 'custom_theme_setup' );
?>