<?php
$page = get_post(8);//Proyecto
?>

            <!-- FORM MODAL COTIZAR -->
            <div class="modal fade jardin-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content cotizar">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3><?php echo get_field('cot_titulo',$page->ID);?></h3>
                <img src="<?php echo get_field('cot_logo_inmobiliaria',$page->ID);?>" alt="">
                <h5 >DEPARTAMENTO TIPO <span class="cot_tipo">A</span></h5>
              </div>
                <div class="modal-body">
  <?php echo do_shortcode( '[contact-form-7 id="20" title="Formulario de Cotización"]' ); ?>
                </div>
            </div><!-- End- modal-content -->

            <!-- COTIZACION -->
              <div class="modal-content cotizacion">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 >COTIZACIÓN</h3>
                  <img class="imagen_56455" src="<?php echo get_template_directory_uri();?>/img/fuenzalida-nav.png" alt="">
                </div>
                <div class="modal-body">
                  <div class="col-xs-12 col-sm-4">
                    <h3>Características</h3>
                    <h5 class="cot_tipo">DEPARTAMENTO TIPO A</h5>
                    <h4>SUPERFICIES APROXIMADAS</h4>
                    <table class="tabla-dpto">
                      <tr>
                        <td>Útil Interior Dpto.</td>
                        <td class="datos-tabla"><span class="cot_superficie_util" >33.11</span> m²</td>
                      </tr>
                      <tr>
                        <td class="">Terrazas</td>
                        <td  class="datos-tabla"><span class="cot_superficie_terraza"></span> m²</td>
                      </tr>
                      <tr>
                        <td colspan="2"><hr></td>
                      </tr>
                      <tr>
                        <td class="totales">Superficie Total</td>
                        <td class="datos-tabla totales"><span class="cot_superficie_total">144.15</span> m²</td>
                      </tr>

                      <tr>
                        <td class="totales cot_clase_depto">Terrazas</td>
                        <td  class="datos-tabla"><span class="cot_superficie_quincho_o_jardin"></span> m²</td>
                      </tr>

                    </table><br>
                    <h5>Formas de pago</h5>
                    <table class="tabla-dpto">
                      <tr>
                        <td class="totales"><b>Precio UF:</b></td>
                        <td class="datos-tabla totales"><b class="cot_precio_uf">1.799</b></td>
                      </tr>
                      <tr>
                        <td>20% Pie:</td>
                        <td class="datos-tabla">UF <span class="cot_precio_pie_20">235</span></td>
                      </tr>
                      <tr>
                        <td>80% Crédito:</td>
                        <td class="datos-tabla">UF <span class="cot_precio_credito">1.425</span></td>
                      </tr>
                    </table>
                    <button class="btn-main-inv" type="button" onClick="printPDFSave();"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span> DESCARGAR PDF</button>
                  </div>

                  <div class="col-xs-12 col-sm-8 text-center">
                    <img class="cot_imagen_cotizacion" src="" alt="Depto">
                  </div>

                  <div class="col-xs-12">
                    <p class="legal"><?php echo get_field('cot_texto_legal',$page->ID);?></p>
                  </div>
                </div>
              </div><!-- End- modal -->
        </div>
      </div>