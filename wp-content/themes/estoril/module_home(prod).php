    <?php
    $page = get_post(6);
    $galeria = get_field('slider_home',$page->ID);
    $galeria_imagenes = explode(",", $galeria);
    ?>

    <!-- MODAL -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalpopup">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <img class="logo-nav" src="<?php echo get_field('logo_estoril',$frontpage_id);?>" alt="Estoril Logo">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>

          <div class="modal-body">
            <h1 class="light">ESTE MES TE REGALAMOS</h1>
            <h1 class="strong"><b>JACUZZI O PISCINA</b></h1>
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <a class="btn-main-inv fix_mobile_23443" href='#target_70' type="button" name="button" data-dismiss="modal" aria-label="Close"><?php echo get_field('texto_boton_quincho',6);?></a>
              </div>
              <div class="col-xs-12 col-sm-6">
                <a class="btn-main-inv fix_mobile_23443" href='#target_69' type="button" name="button" data-dismiss="modal" aria-label="Close"><?php echo get_field('texto_boton_jardin',6);?></a>
                <a href="#bases-legales" class="purple" data-dismiss="modal" aria-label="Close">Ver bases legales</a>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <h6>Promoción válida desde el ... hasta el ...</h6>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="container-fluid">
      <div id="target_<?php echo $page->ID?>" class="row seccion home">
        <div class="col-xs-12 col-sm-12 col-md-8 img-home hidden-xs hidden-sm">
          <!-- Exclusivos Carusel -->
          <div class="owl-carousel owl-theme carousel-exclusivos">

            <?php
              $value = get_field('depthouse_slider_imagen');

              $imagen = wp_get_attachment_image_src( $value ,'full');
              $large = wp_get_attachment_image_src( $value ,'large');
              $medium = wp_get_attachment_image_src( $value ,'medium');
              $thumbnail = wp_get_attachment_image_src( $value ,'thumbnail');
              ?>

            <div class="item">
              <div class="thumbnail">
                <picture>
                  <source media="(max-width: 425px)" srcset="<?php echo $thumbnail[0];?>">
                  <source media="(max-width: 768px)" srcset="<?php echo $medium[0];?>">
                  <source media="(max-width: 1024px)" srcset="<?php echo $large[0];?>">
                  <img class="imagen-carousel" src="<?php echo $imagen[0];?>" alt="Hero">
                </picture>

                <a href="#target_8">
                  <svg class="chevron-down" version="1.1"
                  	 id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                  	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="30px"
                  	 viewBox="0 162.5 612 399" enable-background="new 0 162.5 612 399" xml:space="preserve">
                  <g transform="translate(592.94753,-1935.4889)">
                  	<path d="M10.164,2136.317c4.889,4.907,8.889,9.532,8.889,10.277c0,0.745-68.85,70.204-153,154.353l-153,152.998l-153-152.998
                  		c-84.15-84.149-153-153.623-153-154.386c0-1.506,17.27-19.166,18.743-19.166c0.494,0,65.02,64.123,143.391,142.496
                  		s142.905,142.496,143.409,142.496s65.038-64.123,143.409-142.496S-0.877,2127.395-0.114,2127.395S5.275,2131.41,10.164,2136.317
                  		L10.164,2136.317z"/>
                  </g>
                  </svg>
                </a>
              </div>
            </div>

            <?php
              $value = get_field('penthouse_slider_imagen');

              $imagen = wp_get_attachment_image_src( $value ,'full');
              $large = wp_get_attachment_image_src( $value ,'large');
              $medium = wp_get_attachment_image_src( $value ,'medium');
              $thumbnail = wp_get_attachment_image_src( $value ,'thumbnail');
              ?>

            <div class="item">
              <div class="thumbnail">
                <picture>
                  <source media="(max-width: 425px)" srcset="<?php echo $thumbnail[0];?>">
                  <source media="(max-width: 768px)" srcset="<?php echo $medium[0];?>">
                  <source media="(max-width: 1024px)" srcset="<?php echo $large[0];?>">
                  <img class="imagen-carousel" src="<?php echo $imagen[0];?>" alt="Hero">
                </picture>

                <a href="#target_8">
                  <svg class="chevron-down" version="1.1"
                  	 id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                  	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="30px"
                  	 viewBox="0 162.5 612 399" enable-background="new 0 162.5 612 399" xml:space="preserve">
                  <g transform="translate(592.94753,-1935.4889)">
                  	<path d="M10.164,2136.317c4.889,4.907,8.889,9.532,8.889,10.277c0,0.745-68.85,70.204-153,154.353l-153,152.998l-153-152.998
                  		c-84.15-84.149-153-153.623-153-154.386c0-1.506,17.27-19.166,18.743-19.166c0.494,0,65.02,64.123,143.391,142.496
                  		s142.905,142.496,143.409,142.496s65.038-64.123,143.409-142.496S-0.877,2127.395-0.114,2127.395S5.275,2131.41,10.164,2136.317
                  		L10.164,2136.317z"/>
                  </g>
                  </svg>
                </a>
              </div>
            </div>



          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 info-home">
          <!-- <img class="logo hidden-md hidden-lg" src="" alt="Estoril Logo"> -->

          <a href="#target_69">
            <div class="row zona-depthouse">

              <div class="col-xs-6 dept-mobile hidden-md hidden-lg" style="background:url('<?php echo get_field('dept_house_mobile',$page->ID); ?>') no-repeat" >

              </div>

              <div class="col-xs-6 col-sm-12 col-lg-12 dept">
                <img class="info140" src="<?php echo get_field('depthouse_imagen_derecha',$page->ID);?>" alt="DeptHouse">
                <svg version="1.1"
                	 id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px"
                	 viewBox="125.5 136.5 357 365" enable-background="new 125.5 136.5 357 365" xml:space="preserve">
                    <g transform="translate(0,-952.36218)">
                    	<g transform="translate(20.998031,959.86021)">
                    		<g transform="translate(-11.994093,1.470475)">
                    			<path d="M290.924,145.292V304.46H131.756v12.144h159.168v159.168h12.144V316.603h159.168V304.46H303.068V145.292H290.924z"/>
                    		</g>
                    	</g>
                    </g>
                </svg>
              </div>

            </div>
          </a>

          <div class="row">
            <div class="col-xs-12 franja purple-back">
              <h1><?php echo get_field('texto_franja_home',$page->ID);?></h1>
            </div>
          </div>

          <a href="#target_70"><div class="row zona-penthouse">

            <div class="col-xs-6 col-sm-12 col-lg-12 dept">
              <img class="info140" src="<?php echo get_field('penthouse_imagen_derecha',$page->ID);?>" alt="PentHouse">
              <svg version="1.1"
                 id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px"
                 viewBox="125.5 136.5 357 365" enable-background="new 125.5 136.5 357 365" xml:space="preserve">
                  <g transform="translate(0,-952.36218)">
                    <g transform="translate(20.998031,959.86021)">
                      <g transform="translate(-11.994093,1.470475)">
                        <path d="M290.924,145.292V304.46H131.756v12.144h159.168v159.168h12.144V316.603h159.168V304.46H303.068V145.292H290.924z"/>
                      </g>
                    </g>
                  </g>
              </svg>
            </div>

            <div class="col-xs-6 dept-mobile hidden-md hidden-lg" style="background:url('<?php echo get_field('pent_house_mobile',$page->ID); ?>') no-repeat">

            </div>
          </div></a>

          <!--
          <img class="info140" src="'imagen_texto_bajada'" alt="Info 140m">
          <div class="hline"></div><span class="desde-span">'desde_uf'></span><div class="hline"></div>
          <br><br>
          <a href='#target_69' > <button class="btn-main-inv" type="button" name="button">
            texto_boton_jardin
          </button></a>
          <a href='#target_70' >
          <button class="btn-main-inv" type="button" name="button">
            texto_boton_quincho'
          </button></a> -->

        </div>
      </div>
    </div>
