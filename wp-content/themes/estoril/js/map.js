
<?php
  $page_entorno = get_post(10);
  $pin = get_field('pin',$page_entorno->ID);
  $direccion = get_field('direccion',$page_entorno->ID);
  $arr = array('post_type'=>'marcadores','order_by'=>'date','order'=>'DESC','post_status'=>'publish','posts_per_page'=>-1);
  $marcadores = get_posts( $arr );
  //var_dump($marcadores);
?>
function initMap() {
  initMapfull('map');
}

function initMapfull (id) {
  var map = new google.maps.Map(document.getElementById(id), {
    center: {lat: <?php echo $direccion['lat'];?>, lng: <?php echo $direccion['lng'];?>},
    zoom: 14,
    scrollwheel: false,
    styles:[
    {
        "featureType": "all",
        "elementType": "all",
        "stylers": [
            {
                "gamma": "0.83"
            },
            {
                "saturation": "-86"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "geometry",
        "stylers": [
            {
                "saturation": "-31"
            },
            {
                "lightness": "15"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "saturation": "-99"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels",
        "stylers": [
            {
                "saturation": "-63"
            },
            {
                "gamma": "1.14"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text",
        "stylers": [
            {
                "gamma": "0.92"
            },
            {
                "saturation": "-75"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "gamma": "1.29"
            },
            {
                "saturation": "-6"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "gamma": "1.00"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "weight": "0.01"
            },
            {
                "gamma": "0.00"
            },
            {
                "lightness": "-29"
            },
            {
                "saturation": "-82"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "all",
        "stylers": [
            {
                "gamma": "1.00"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-32"
            },
            {
                "lightness": "-8"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#54274e"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [
            {
                "saturation": "2"
            },
            {
                "lightness": "-13"
            },
            {
                "gamma": "8.46"
            },
            {
                "weight": "2.14"
            },
            {
                "invert_lightness": true
            }
        ]
    }
]
  });

  window.marker_collector = [];

  var marker = new google.maps.Marker({
    position: {lat: <?php echo $direccion['lat'];?>, lng: <?php echo $direccion['lng'];?>},
    map: map,
    icon: '<?php echo $pin;?>',
    title: 'Estoril',
    });

  <?php
  $old_clase = "";
  $clase="";
  $marcador_clase = "";
  foreach( $marcadores AS $key=>$value ){
    $direccion = get_field('geo',$value->ID);
    //var_dump($direccion);
      $clase = get_field('clase',$value->ID);
    if ( $clase ){

      $marcador_clase[$clase].=$value->ID.",";

      ?>
        if (!window.marker_collector['<?php echo get_field('clase',$value->ID); ?>']){ window.marker_collector['<?php echo get_field('clase',$value->ID); ?>']=""; }
        window.marker_collector['<?php echo get_field('clase',$value->ID); ?>'] = window.marker_collector['<?php echo get_field('clase',$value->ID); ?>']+"<?php echo $value->ID;?>;";
      <?php
      $old_clase = $clase;
    }
   
  ?>

  

  window.marker_<?php echo $value->ID; ?> = new google.maps.Marker({
    position: {lat: <?php echo $direccion['lat'];?>, lng: <?php echo $direccion['lng'];?>},
    map: map,
    icon: '<?php echo get_field('pin',$value->ID);?>',
    title: '<?php echo get_field('titulo',$value->ID);?>',
  });


  var contentString = "<?php echo get_field('texto',$value->ID);?>";
  window.infowindow_<?php echo $value->ID; ?> = new google.maps.InfoWindow({
    content: contentString
  });

  window.marker_<?php echo $value->ID; ?>.addListener('click', function() {
    window.infowindow_<?php echo $value->ID; ?>.open(map, window.marker_<?php echo $value->ID; ?>);

    window.marker_<?php echo $value->ID; ?>.setAnimation(google.maps.Animation.DROP);
    //setTimeout(function(){ marker_<?php echo $value->ID; ?>.setAnimation(null); }, 1050);

  });

  window.marker_<?php echo $value->ID; ?>.setAnimation(google.maps.Animation.DROP);

  <?php

  }
  //Colegios
  $colegios = explode(",",$marcador_clase['colegios']);
  $supermercados = explode(",",$marcador_clase['supermercados']);
  $universidades = explode(",",$marcador_clase['universidades']);
  $bancos = explode(",",$marcador_clase['bancos']);
  $restaurantes = explode(",",$marcador_clase['restaurantes']);
  $clinicas = explode(",",$marcador_clase['clinicas']);
  $iglesias = explode(",",$marcador_clase['iglesias']);
  ?>
}

function bounceMarker(clase){

  if ( clase == 'stop' ){

    <?php
    foreach($colegios AS $item){
      if (!$item){ continue; }

    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.STOP);
    window.infowindow_<?php echo $item; ?>.close(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>

    <?php
    foreach($supermercados AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.STOP);
    window.infowindow_<?php echo $item; ?>.close(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>

    <?php
    foreach($universidades AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.STOP);
    window.infowindow_<?php echo $item; ?>.close(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>

    <?php
    foreach($bancos AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.STOP);
    window.infowindow_<?php echo $item; ?>.close(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>

  
    <?php
    foreach($restaurantes AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.STOP);
    window.infowindow_<?php echo $item; ?>.close(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>

    <?php
    foreach($clinicas AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.STOP);
    window.infowindow_<?php echo $item; ?>.close(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>

    <?php
    foreach($iglesias AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.STOP);
    window.infowindow_<?php echo $item; ?>.close(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>

  }

  if ( clase == 'colegios' ){
    <?php
    foreach($colegios AS $item){
      if (!$item){ continue; }

    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.BOUNCE);
    window.infowindow_<?php echo $item; ?>.open(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>
  }

  if ( clase == 'supermercados'){
    <?php
    foreach($supermercados AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.BOUNCE);
    window.infowindow_<?php echo $item; ?>.open(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>
  }

  if ( clase == 'universidades' ){
    <?php
    foreach($universidades AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.BOUNCE);
    window.infowindow_<?php echo $item; ?>.open(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>
  }

  if ( clase == 'bancos'){
    <?php
    foreach($bancos AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.BOUNCE);
    window.infowindow_<?php echo $item; ?>.open(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>
  }

  if ( clase == 'restaurantes'){
    <?php
    foreach($restaurantes AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.BOUNCE);
    window.infowindow_<?php echo $item; ?>.open(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>
  }

  if ( clase == 'clinicas'){
    <?php
    foreach($clinicas AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.BOUNCE);
    window.infowindow_<?php echo $item; ?>.open(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>
  }

  if ( clase == 'iglesias'){
    <?php
    foreach($iglesias AS $item){
      if (!$item){ continue; }
    ?>
    window.marker_<?php echo $item; ?>.setAnimation(google.maps.Animation.BOUNCE);
    window.infowindow_<?php echo $item; ?>.open(map, window.marker_<?php echo $item; ?>);
    <?php
    }
    ?>
  }




}
