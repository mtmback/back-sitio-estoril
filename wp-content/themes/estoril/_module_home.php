    <?php
    $page = get_post(6);
    $galeria = get_field('slider_home',$page->ID);
    $galeria_imagenes = explode(",", $galeria);
    ?>
    <div class="container-fluid">
      <div id="target_<?php echo $page->ID?>" class="row seccion home">
        <div class="col-xs-12 col-sm-12 col-md-8 img-home">
          <!-- Exclusivos Carusel -->
          <div class="owl-carousel owl-theme carousel-exclusivos">
            <?php
            foreach ($galeria_imagenes as $key => $value) {
              $imagen = wp_get_attachment_image_src( $value ,'large');
              ?>

            <div class="item">
              <div class="thumbnail">
                <img src="<?php echo $imagen[0];?>" alt="Exclusivos">
              </div>
            </div>

              <?php
            }
            ?>

          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 info-home">
          <img class="logo" src="<?php echo get_field('imagen_estoril',$page->ID);?>" alt="Estoril Logo">
          <img class="info140" src="<?php echo get_field('imagen_texto_bajada',$page->ID);?>" alt="Info 140m">
          <div class="hline"></div><span><?php echo get_field('desde_uf',$page->ID);?></span><div class="hline"></div>
          <br><br>
          <a href='#target_69' class="btn-main-inv" type="button" name="button"><?php echo get_field('texto_boton_jardin',$page->ID);?></a>
          <a href='#target_70' class="btn-main-inv" type="button" name="button"><?php echo get_field('texto_boton_quincho',$page->ID);?></a>
        </div>
      </div>
    </div>