<?php
$frontpage_id = get_option( 'page_on_front' );
?>



<footer class="footer">
    <div class="col-xs-12 col-sm-12 col-md-4 ">
       <img class="logo-right" src="<?php echo get_field('fuenzalida_comercial',$frontpage_id);?>" alt="">
       <p><?php echo get_field('telefono',$frontpage_id);?><br>
       <?php echo get_field('email',$frontpage_id);?></p>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 text-center">
      <img class="logo-left" src="<?php echo get_field('fuenzalida_inmobiliaria',$frontpage_id);?>" alt="Fuenzalida">
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 text-left ">
      <h4><b><?php echo get_field('titulo_sala_de_ventas',$frontpage_id);?></b></h4>
      <p><b><?php echo get_field('direccion',$frontpage_id);?></b><br>
      <?php echo get_field('horario',$frontpage_id);?></p>
    </div>
  </footer>



  <!-- JS/JQUERY FILES -->
  <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/lib/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/lib/OwlCarousel2-2.2.1/dist/owl.carousel.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.elevateZoom-3.0.8.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.maskedinput.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.rut.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/scroll-anywhere.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC22J3P1kEUP80JLKyyyCdkJVXcosKdu7o&callback=initMap" async defer></script>

<script type="text/javascript">
  //MODAL navidad
  var inst = $('#modalNavidad').remodal();
  /* Opens the modal window */
  inst.open();
  /* Closes the modal window */
  //inst.close();
</script>

  <?php
    $page_entorno = get_post(10);
    $pin = get_field('pin',$page_entorno->ID);
    $direccion = get_field('direccion',$page_entorno->ID);
    $arr = array('post_type'=>'marcadores','order_by'=>'date','order'=>'DESC','post_status'=>'publish','posts_per_page'=>-1);
    //$marcadores = get_posts( $arr );
    //var_dump($marcadores);
  ?>

    <script type="text/javascript" >
      <?php include('js/map.js'); ?>
    </script>

    <script type="text/javascript">

    /* VALIDACIÓN MASCARA */
    //$("#telefono").mask("99 9 9999 9999",{placeholder:""});

    /* VALIDACION RUT */

    $("#contacto_rut, #cotizar_rut").rut({
       formatOn: 'keyup blur',
       minimumLength: 8, // validar largo mínimo; default: 2
       validateOn: 'change keyup' // si no se quiere validar, pasar null
       });


    </script>
    <script>
    var myWidth = window.innerWidth;
    /* ANIMACIONES SIDENAV */
      function openNav() {
        if (myWidth < 770){
          document.getElementById("mySidenav").style.width = "240px";
        }
        else {
          document.getElementById("mySidenav").style.width = "320px";
          document.getElementById("main").style.marginLeft = "320px";
        }
        $( ".sidenav a" ).delay(200).animate({
          opacity: 1
        }, 200);
      }

      function closeNav() {
        $( ".sidenav a" ).animate({
          opacity: 0
        }, 10);
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
      }

    /* OWL CAROUSEL */
    $('.owl-carousel.carousel-modelos').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    navText: ["<img src='<?php echo get_template_directory_uri();?>/img/chevron-izq.svg' alt='Anterior'>","<img src='<?php echo get_template_directory_uri();?>/img/chevron-der.svg' alt='Siguiente'>"],
    responsive:{
        0:{
            items:1
        },
        900:{
            items:3
        }
      }
    });
    $('.owl-carousel.carousel-vermas').owlCarousel({
    loop:false,
    dots:true,
    nav:false,
    margin:10,
    responsive:{
        0:{
            items:1
        },
        900:{
            items:3
        }
      }
    });

    $('.owl-carousel.carousel-exclusivos').owlCarousel({
    loop:false,
    dots:false,
    nav:false,
    margin:10,
    mouseDrag:false,
    touchDrag: false,
    animateOut: 'fadeOut',
    responsive:{
        0:{
            items:1
        }
      }
    });
    var owl = $('.owl-carousel.carousel-exclusivos');
    owl.owlCarousel();
    // Go to the next item
    $('.zona-penthouse').hover(function() {
        owl.trigger('to.owl.carousel', 1);
    })
    $('.zona-depthouse').hover(function() {
        owl.trigger('to.owl.carousel', 0);
    })


    /* ANIMACIÓN COTIZACIÓN */
    function hideCotizar(){
        $(".cotizar").fadeOut(function() {
          $(".cotizacion").fadeIn();
          });
      };
    function hideCotizacion() {
      $(".cotizacion").fadeOut();
      $(".cotizar").fadeIn();
      };

    /* Animacion Flip */
    function flipdiv(id) {
      document.querySelector(id).classList.toggle("flip");
    }

      /* if ($(window).width() <= 768) {
        $('.flip-container').addClass('flip');
      } */


    /* VISTAS MODELOS */
    function verModelo(clase){
      $('.vista-modelos.'+clase).animate({'left' : '0%'}, {duration : 400});
      $('.vista-modelos.'+clase).css('display', 'block').css('position', 'relative');
      $('.modelos.'+clase).animate({'right' : '-100%'}, {duration : 400});
      $('.modelos.'+clase).css('display', 'none');
    }
    function cerrarModelo(clase) {
      $('.vista-modelos.'+clase).css('position', 'absolute');
      $('.vista-modelos.'+clase).animate({'left' : '-100%'}, {duration : 400});
      $('.vista-modelos.'+clase).css('display', 'none');
      $('.modelos.'+clase).css('display', 'block');
      $('.modelos.'+clase).animate({'right' : '0%'}, {duration : 400});
    }

    //CUSTOM SCROLL
    $(window).ready(function(){
      (function($){
          $(window).on("load",function(){
              $(".back").mCustomScrollbar();
          });
          $(window).resize(function(){
              $(".back").mCustomScrollbar();
          });
      })(jQuery);
    });

    $(document).ready(function(){
      if ($( window ).width() < 425 ){
        $('.dept-mobile').css({'min-height' : $('.zona-depthouse').height()});
        $('.dept').css({'min-height' : $('.zona-depthouse').height()});
      }
      $('.imagen-carousel').css({'max-height' : $('.info-home').height()-30});
    });

    $(window).resize(function(){
      if ($( window ).width() < 425 )
      {
        $('.dept-mobile').css({'min-height' : $('.zona-depthouse').height()});
        $('.dept').css({'min-height' : $('.zona-depthouse').height()});
        }
      $('.imagen-carousel').css({'max-height' : $('.info-home').height()-30});
    });

    </script>


    <script src="<?php echo get_template_directory_uri();?>/lib/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript">
      function backCotizar( id , qj){
        //--- deptos info-- INI
        var depto = [];
        var email = [];
        <?php
        $arr = array( 'post_type'=>['depto_jardin','dpto_quincho'], 'order_by'=>'date', 'order'=>'DESC', 'post_status'=>'publish','posts_per_page'=>-1 );
        $deptos = get_posts($arr);

        foreach ($deptos as $key => $value) {
          $depto_info = "";
          $depto_info.= $value->ID."|";
          $depto_info.= get_field('imagen_home',$value->ID)."|";
          $depto_info.= get_field('titulo',$value->ID)."|";
          $depto_info.= get_field('tipo',$value->ID)."|";
          $depto_info.= get_field('dormitorios',$value->ID)."|";
          $depto_info.= get_field('banos',$value->ID)."|";
          $depto_info.= get_field('cocina',$value->ID)."|";
          $depto_info.= get_field('quincho_o_jardin',$value->ID)."|";
          $depto_info.= get_field('superficie_util',$value->ID)."|";
          $depto_info.= get_field('superficie_total',$value->ID)."|";
          $depto_info.= get_field('superficie_quincho_o_jardin',$value->ID)."|";
          $depto_info.= get_field('precio_uf',$value->ID)."|";
          $depto_info.= get_field('precio_pie_20',$value->ID)."|";
          $depto_info.= get_field('precio_credito',$value->ID)."|";
          $depto_info.= get_field('imagen_cotizacion',$value->ID)."|";
          $depto_info.= get_field('superficie_terraza',$value->ID)."|";
          $depto_info = str_replace("'", "", $depto_info);
          //-----
          $depto_info_email = "";

          $depto_info_email.= "titulo : ".get_field('titulo',$value->ID)."\\n";
          $depto_info_email.= "tipo : ".get_field('tipo',$value->ID)."\\n";
          $depto_info_email.= "dormitorios : ".get_field('dormitorios',$value->ID)."\\n";
          $depto_info_email.= "baños : ".get_field('banos',$value->ID)."\\n";
          $depto_info_email.= "cocina : ".get_field('cocina',$value->ID)."\\n";
          $depto_info_email.= "quincho_o_jardin : ".get_field('quincho_o_jardin',$value->ID)."\\n";
          $depto_info_email.= "superficie_util : ".get_field('superficie_util',$value->ID)."\\n";
          $depto_info_email.= "superficie_terraza : ".get_field('superficie_terraza',$value->ID)."\\n";
          $depto_info_email.= "superficie_total : ".get_field('superficie_total',$value->ID)."\\n";
          $depto_info_email.= "superficie_quincho_o_jardin : ".get_field('superficie_quincho_o_jardin',$value->ID)."\\n";
          $depto_info_email.= "precio_uf : ".get_field('precio_uf',$value->ID)."\\n";
          $depto_info_email.= "precio_pie_20 : ".get_field('precio_pie_20',$value->ID)."\\n";
          $depto_info_email.= "precio_credito : ".get_field('precio_credito',$value->ID)."\\n";
          $depto_info_email.= "imagen_cotizacion : ".get_field('imagen_cotizacion',$value->ID)."\\n";
          $depto_info_email = str_replace("'", "", $depto_info);
          $depto_info_email.= get_field('imagen_home',$value->ID)."\\n";

          ?>

          depto[<?php echo $value->ID ?>] = '<?php echo $depto_info; ?>';
          email[<?php echo $value->ID ?>] = '<?php echo $depto_info_email; ?>';
          <?php
        }
        ?>
        //--- deptos info-- END
        var selected_depto = depto[id];

        var depto = selected_depto.split('|');
        window.titulo = depto[2];
        window.tipo = depto[3];
        window.texto_bajada = depto[4];
        window.dormitorios = depto[4];
        window.banos = depto[5];
        window.cocina = depto[6];
        window.quincho_o_jardin = depto[7];
        window.superficie_util = depto[8];
        window.superficie_total = depto[9];
        window.superficie_quincho_o_jardin = depto[10];
        window.precio_uf = depto[11];
        window.precio_pie_20 = depto[12];
        window.precio_credito = depto[13];
        window.imagen_cotizacion = depto[14];
        window.superficie_terraza = depto[15];
        getBlob(window.imagen_cotizacion,<?php echo $value->ID; ?>);
        if (qj == 'quincho'){
          window.clase_depto = "Quincho";
        }else{
          window.clase_depto = "Jadín";
        }

        $('.cot_titulo').html(window.titulo);
        $('.cot_tipo').html(window.tipo);
        $('.cot_dormitorios').html(window.dormitorios);
        $('.cot_banos').html(window.banos);
        $('.cot_cocina').html(window.cocina);
        $('.cot_quincho_o_jardin').html(window.quincho_o_jardin);
        $('.cot_superficie_util  ').html(window.superficie_util);
        $('.cot_superficie_total').html(window.superficie_total);
        $('.cot_superficie_terraza').html(window.superficie_terraza);
        $('.cot_superficie_quincho_o_jardin').html(window.superficie_quincho_o_jardin);
        $('.cot_precio_uf').html(window.precio_uf);
        $('.cot_precio_pie_20').html(window.precio_pie_20);
        $('.cot_precio_credito').html(window.precio_credito);
        $('.cot_imagen_cotizacion').attr('src', window.imagen_cotizacion);
        $('.cot_clase_depto').html( window.clase_depto);
        $('#cotizador_informacion').html( email[<?php echo $value->ID ?>]);

      }


window.imagen_blob = [];

function getBlob( url, id ) {
  console.log(url+" : "+id);
  window.image_id=id;
  var img = new Image();
  img.setAttribute('crossOrigin', 'anonymous');
  img.onload = function ( id ) {
      var canvas = document.createElement("canvas");
      canvas.width =this.width;
      canvas.height =this.height;
      var ctx = canvas.getContext("2d");
      ctx.drawImage(this, 0, 0);
      var dataURL = canvas.toDataURL("image/png");
      window.imagen_blob[window.image_id]=dataURL;
      console.log('-->'+window.image_id);
      //console.log(dataURL);
     //return (dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
  };
  img.src = url;
}


function printPDFSave(){

  var doc = new jsPDF();
  //var html = $('#cotizacion_departamento_pdf').html();

  doc.text(20, 40, 'Cotización');
  doc.setFontSize(12);
//LOGOS
  doc.addImage(window.logo_estoril_logo, 'PNG', 0, 0, 0, 0);


  doc.text(20, 50, 'Características');

  doc.text(20, 60, 'Modelo:');
  doc.text(80, 60, 'Tipo - '+window.tipo);

  doc.text(20, 65, 'Dormitorios:');
  doc.text(80, 65,  window.dormitorios.toString() );

  doc.text(20, 70, 'Baños:');
  doc.text(80, 70, window.banos.toString() );


  doc.text(20, 80, 'Superficie útil:');
  doc.text(80, 80, window.superficie_util.toString()+"m2" );
  doc.text(20, 85, 'Superficie Terrazas:');
  doc.text(80, 85, window.superficie_terraza.toString()+"m2" );
  doc.text(20, 90, 'Total:');
  doc.text(80, 90, window.superficie_total.toString()+"m2" );
  doc.text(20, 95, window.clase_depto+':');
  doc.text(80, 95, window.superficie_quincho_o_jardin.toString()+"m2" );



  doc.text(20, 100, 'Precio UF:');
  doc.text(80, 100, window.precio_uf.toString() );
  doc.text(20, 105, 'Precio Pie 20% UF:');
  doc.text(80, 105, window.precio_pie_20.toString() );
  doc.text(20, 110, 'Precio Crédito 80% UF:');
  doc.text(80, 110, window.precio_credito.toString() );


  //doc.addPage();
  //doc.addImage(imagen_blob,0,0,800,600);
  doc.addImage(imagen_blob[window.image_id], 'PNG', 25, 130, 0, 0);
  doc.setFontSize(9);
/*
  doc.text(20, 250, 'Sala de Ventas:');
  doc.text(80, 250, 'Antonio Bellet 183, Providencia, Chile.');
  doc.text(80, 255, 'Lunes a Domingo de 11:00 a 14:00 hrs y de 15:30 a 19:00 hrs.');
  doc.text(20, 260, 'Teléfono:');
  doc.text(80, 260, '+56 9 5655 1825');
  doc.text(20, 265, 'Email:');
  doc.text(80, 265, 'walvarez@fdi.cl');
  */
  doc.text(20, 250, '*Legal: Precios referenciales, dependiendo de orientación y piso. Para mayor información consulte en sala de ventas.');
  //doc.addImage(window.logo_fuenzalida_black, 'PNG', 0, 270, 0, 0);
  doc.addImage(window.window.logo_estoril_footer, 'PNG', 0, 260, 0, 0);
  doc.save('Cotizacion.pdf');
}

// ANALITICA     jq$('#contacto_nombre').on('click',function(){ gzz('send', 'event', 'contacto','input_Nombre'); console.log('contacto nombre'); });

$('#contacto_nombre').on('click',function(){  ga('send', 'event', 'Contacto', 'input_Nombre'); });
$('#contacto_apellido').on('click',function(){  ga('send', 'event', 'Contacto', 'input_Apellido'); });
$('#contacto_telefono').on('click',function(){  ga('send', 'event', 'Contacto', 'input_Telefono'); });
$('#contacto_email').on('click',function(){  ga('send', 'event', 'Contacto', 'input_Email'); });
$('#contacto_comentario').on('click',function(){  ga('send', 'event', 'Contacto', 'input_Comentario'); });
//----
$('#cotizar_nombre').on('click',function(){  ga('send', 'event', 'Cotizador', 'input_Nombre'); });
$('#cotizar_apellido').on('click',function(){  ga('send', 'event', 'Cotizador', 'input_Apellido'); });
$('#cotizador_email').on('click',function(){  ga('send', 'event', 'Cotizador', 'input_Email'); });
$('#cotizador_telefono').on('click',function(){  ga('send', 'event', 'Cotizador', 'input_Telefono'); });
$('#cotizador_comentario').on('click',function(){  ga('send', 'event', 'Cotizador', 'input_Comentario'); });
//----
function onSend(){
  ga('send', 'event', 'Cotizador', 'envioExito','Tipo:'+window.tipo);

}
    </script>
    <?php wp_footer();?>
  </body>
</html>
