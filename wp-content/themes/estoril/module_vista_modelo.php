            <?php
              foreach ($deptos  as $key => $value) {
                $galeria = get_field('galeria',$value->ID);
                $galeria_imagenes = explode(",", $galeria);
                $galeria_b = get_field('galeria_20',$value->ID);
                ?>
      <!-- VISTA MODELOS JARDIN-->
      <div id="vista-modelos_<?php echo $value->ID; ?>" class="row seccion vista-modelos modal_modelo gray-back">
      <div class="inivx"><div>
        
          
        
        <span class="close-modal eresr" onclick="$('#vista-modelos_<?php echo $value->ID; ?>').fadeOut();">&times;</span>

        <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
            <h2 class="purple">MODELO</h2>
            <h3 class="purple "><?php echo get_field( 'tipo',$value->ID); ?></h3>

            <ul>
              <li class=""><?php echo get_field( 'dormitorios',$value->ID); ?> · <?php echo get_field( 'banos',$value->ID); ?></li>
              <li class=""><?php echo get_field( 'cocina',$value->ID); ?></li>
              <li class=""><?php echo get_field( 'quincho_o_jardin',$value->ID); ?></li>
            </ul>

            <table class="tabla-modelos">

              <tr>
                <td>Sup. Útil</td>
                <td class="valor"><span class=""><?php echo get_field( 'superficie_util',$value->ID); ?></span> m²</td>
              </tr>
              <tr>
                <td>Sup. Terraza</td>
                <td class="valor"><span class=""><?php echo get_field( 'superficie_terraza',$value->ID); ?></span> m²</td>
              </tr>
              <tr>
                <td colspan="2"><hr></td>
              </tr>
              <tr class="total">
                <td>Sup. Total</td>
                <td class="valor"><span class=""><?php echo get_field( 'superficie_total',$value->ID); ?></span> m²</td>
              </tr>
              <tr class="total">
                <td>Sup. <x class="identificador_clase"><?php echo $identificador_clase; ?></x></td>
                <td class="valor"><span class=""><?php echo get_field( 'superficie_quincho_o_jardin',$value->ID); ?></span> m²</td>
              </tr>
            </table>
            <button onclick="hideCotizacion(); $('#vista-modelos_<?php echo $value->ID; ?>').slideUp();" type="submit" class="btn-main-inv" data-toggle="modal" data-target=".jardin-modal-lg">COTIZAR</button>

            <?php //include('module_cotizacion.php'); ?>
        </div>

        <!-- CAROUSEL COTIZAR JARDÍN -->
        <div class="col-xs-12 col-sm-6 col-md-10 col-lg-10 text-center fix_modal_76877768">
          <!-- Carousel ver+ -->
          <div class="owl-carousel owl-theme carousel-vermas fix_modal_76877768 ">

            <?php

            foreach ($galeria_b as $key_img => $value) {

              
              
            ?>
              <div class="item">
                <div class="thumbnail">
                  <h4><?php echo $value['titulo']; ?></h4>
                  <p><?php echo $value['bajada']; ?></p>
                  <img src="<?php echo $value['imagen']['sizes']['medium_large']; ?>" alt=""><br>
                  <a href="" data-toggle="modal" data-target="#jardin_<?php echo $value['imagen']['ID']; ?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
                </div>
              </div>
            <?php            
            }
            ?>

          </div>
            <?php
            foreach ($galeria_b as $key_img => $img) {
              //$imagen = wp_get_attachment_image_src( $img ,'large')[0];
            ?>

          <!-- Modal -->
          <div class="modal fade" id="jardin_<?php echo $img['imagen']['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <img src="<?php echo $img['imagen']['sizes']['medium_large']; ?>" alt="">
              </div>
            </div>
          </div><!-- End-Modal -->

            <?php            
            }
            ?>

        </div>
      </div></div class="endvx">
      </div>

      <?php
      }
      ?>