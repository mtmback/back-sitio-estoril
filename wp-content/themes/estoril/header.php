<?php
$frontpage_id = get_option( 'page_on_front' );
$menu = estoril_wpGetMenuArray('header-menu');
$ran = rand(1,99);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0">
    <link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri();?>/img/favicon-estoril.ico">
    <title>Estoril | 820</title>
    <!-- STYLESHEETS -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/lib/OwlCarousel2-2.2.1/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/lib/OwlCarousel2-2.2.1/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/lib/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/modal.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/back.css?<?php echo $ran;?>">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css">

    <?php wp_head(); ?>

    <?php echo get_field('analytics_script',$frontpage_id);?>

    <script type="text/javascript">
      getGenericBlob( "" , 1 );
      getGenericBlob( "" , 2 );

      function getGenericBlob( url , img_n) {
        if (img_n==1){
          url = "<?php echo get_template_directory_uri();?>/img/pdf_estoril_head.png";
        }

        if (img_n==2){
          url = "<?php echo get_template_directory_uri();?>/img/pdf_estoril_footer.png";
        }

          var img = new Image();

          img.setAttribute('crossOrigin', 'anonymous');

          img.onload = function () {
              var canvas = document.createElement("canvas");
              canvas.width =this.width;
              canvas.height =this.height;

              var ctx = canvas.getContext("2d");
              ctx.drawImage(this, 0, 0);

              var dataURL = canvas.toDataURL("image/png");
              //window.logo_lapoza_logo = (dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));

        if (img_n==1){
          //url = "<?php echo get_template_directory_uri();?>/img/logoboth2.png";
          window.logo_estoril_logo = dataURL;
        }

        if (img_n==2){
          window.logo_estoril_footer = dataURL;
        }


           //   return (dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));

              //return dataURL;
          };

          img.src = url;
      }
    </script>
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">

        <div class="vaina-mobile">
          <div class="col-xs-6">
            <img class="burger-menu" src="<?php echo get_template_directory_uri();?>/img/menu.svg" alt="" onclick="openNav()">
          </div>
          <div class="col-xs-6 zona-del-logo">
            <div class="navbar-header navbar-right">
              <img class="logo-nav" src="<?php echo get_field('logo_estoril',$frontpage_id);?>" alt="Estoril Logo">
            </div>
          </div>
        </div>

        <div class="resto">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header navbar-right">
            <img class="logo-nav" src="<?php echo get_field('logo_estoril',$frontpage_id);?>" alt="Estoril Logo">
            <!-- <img class="fuenzalida-nav" src="<?php echo get_template_directory_uri();?>/img/fuenzalida-nav.png" alt=""> -->
          </div>

          <img class="burger-menu" src="<?php echo get_template_directory_uri();?>/img/menu.svg" alt="" onclick="openNav()">
        </div>

      </div><!-- /.container-fluid -->
    </nav>


    <div id="mySidenav" class="sidenav">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

      <?php
        foreach ($menu as $key => $value) {
          ?>
          <a class="enlaceSidenav" href="#target_<?php echo $value['object_id']; ?>" onClick="closeNav();" ><?php echo $value['title']; ?></a>
          <?php
        }
      ?>
    </div>
