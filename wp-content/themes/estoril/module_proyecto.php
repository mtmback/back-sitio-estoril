<?php
$page = get_post(8);
$galeria = get_field('galeria',$page->ID);
$galeria_imagenes = explode(",", $galeria);
$arr = array( 'post_type'=>'profesionales', 'order_by'=>'date', 'order'=>'DESC', 'post_status'=>'publish' );
$profesionales = get_posts($arr);
?>
    <div id="target_<?php echo $page->ID?>"  class="container-fluid">
    <img class="depto-diseno" src="<?php echo get_template_directory_uri();?>/img/PROYECTO.png" alt="">
    <div id="exclusivos" class="seccion exclusivos">

      <div class="row gray-back">
        <div class="col-xs-12 col-sm-12 col-md-5">
          <h1 class="purple"><?php echo get_field('titulo',$page->ID);?></h1>
          <h2 class="purple"><?php echo get_field('subtitulo',$page->ID);?></h2>
          <p>
            <?php echo get_field('caracteristicas',$page->ID);?></p>
          <p>
          <?php echo get_field('terminaciones',$page->ID);?></p>
          <p>
          <?php echo get_field('entorno_y_ubicacion',$page->ID);?> </p>

          <div class="panel-group arquitectos" id="accordion" role="tablist" aria-multiselectable="true">
      <?php
      $cont = 1;
      foreach ($profesionales as $key => $value) {
        ?>

            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $cont; ?>" aria-expanded="true" aria-controls="collapseOne">
                    <p><b><?php echo get_field('area',$value->ID);?></b><br>
                    <?php echo get_field('nombre',$value->ID);?></p>
                    <svg class="chevron-down" version="1.1"
                    	 id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                    	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="30px"
                    	 viewBox="0 162.5 612 399" enable-background="new 0 162.5 612 399" xml:space="preserve">
                    <g transform="translate(592.94753,-1935.4889)">
                    	<path d="M10.164,2136.317c4.889,4.907,8.889,9.532,8.889,10.277c0,0.745-68.85,70.204-153,154.353l-153,152.998l-153-152.998
                    		c-84.15-84.149-153-153.623-153-154.386c0-1.506,17.27-19.166,18.743-19.166c0.494,0,65.02,64.123,143.391,142.496
                    		s142.905,142.496,143.409,142.496s65.038-64.123,143.409-142.496S-0.877,2127.395-0.114,2127.395S5.275,2131.41,10.164,2136.317
                    		L10.164,2136.317z"/>
                    </g>
                    </svg>
                  </a>
                </h4>
              </div>
              <div id="collapse<?php echo $cont; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <p><?php echo get_field('descripcion',$value->ID);?></p>
                </div>
              </div>
            </div>

        <!--
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
          <div id="flip_<?php echo $value->ID; ?>" class="flip-container">
            <div class="flipper">
              <div class="front">
                <p><b><?php echo get_field('area',$value->ID);?></b><br>
                <?php echo get_field('nombre',$value->ID);?></p>
              </div>
              <div class="back toggle-on purple-back fix_mobile_3434">
                <p><b><?php echo get_field('area',$value->ID);?></b><br>
                <?php echo get_field('nombre',$value->ID);?></p>
                <p><?php echo get_field('descripcion',$value->ID);?></p>
              </div>
            </div>
          </div>
          <button onclick="flipdiv('#flip_<?php echo $value->ID; ?>')" class="btn-ver" type="button" name="button">VER +</button>
        </div> -->


        <?php
        $cont+=1;
      }

      ?>
    </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7">
          <!-- Exclusivos Carusel -->
          <div class="owl-carousel owl-theme carousel-exclusivos">
            <?php
            foreach ($galeria_imagenes as $key => $value) {
              $imagen = wp_get_attachment_image_src( $value ,'full');
              $large = wp_get_attachment_image_src( $value ,'large');
              $medium = wp_get_attachment_image_src( $value ,'medium');
              $thumbnail = wp_get_attachment_image_src( $value ,'thumbnail');
              ?>

            <div class="item">
              <div class="thumbnail">

                <picture>
                  <source media="(max-width: 768px)" srcset="<?php echo $medium[0];?>">
                  <source media="(max-width: 1024px)" srcset="<?php echo $large[0];?>">
                  <img src="<?php echo $imagen[0];?>" width="1500" height="400" alt="Hero">
                </picture>

              </div>

            </div>

              <?php
            }
            ?>

          </div>
          <a class="btn-main-inv fix_mobile_23443" href='#target_70' type="button" name="button"><?php echo get_field('texto_boton_quincho',6);?></a>

          <a class="btn-main-inv fix_mobile_23443" href='#target_69' type="button" name="button"><?php echo get_field('texto_boton_jardin',6);?></a>

        </div>
      </div>



    </div>
  </div>
