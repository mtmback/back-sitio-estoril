<?php
$page = get_post(8);//Proyecto
$dj_galeria = get_field('dj_galeria',$page->ID);
$dj_galeria_imagenes = explode(",", $dj_galeria);
$identificador_clase="Jardín";
$arr = array( 'post_type'=>'depto_jardin', 'order_by'=>'date', 'order'=>'DESC', 'post_status'=>'publish' );
$deptos = get_posts($arr);

?>
  <div class="container-fluid">
  <div id="exclusivos" class="seccion exclusivos">
    <img class="depto-diseno" src="<?php echo get_template_directory_uri();?>/img/JARDIN.png" alt="">
    <div class="row gray-back">
      <div class="col-xs-12 col-sm-12 col-md-5">
        <h1 class="purple"><?php echo get_field('dj_titulo',$page->ID);?></h1>
        <h2 class="purple"><?php echo get_field('dj_subtitulo',$page->ID);?></h2>
        <p>
          <?php echo get_field('dj_caracteristicas',$page->ID);?></p>

        <div id="accordion" class="panel-group" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div id="headingOne" class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a role="button" href="#collapseOne" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">
                  <p><b>Terminaciones:</b></p>
                  <svg class="chevron-down" version="1.1"
                     id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="30px"
                     viewBox="0 162.5 612 399" enable-background="new 0 162.5 612 399" xml:space="preserve">
                  <g transform="translate(592.94753,-1935.4889)">
                    <path d="M10.164,2136.317c4.889,4.907,8.889,9.532,8.889,10.277c0,0.745-68.85,70.204-153,154.353l-153,152.998l-153-152.998
                      c-84.15-84.149-153-153.623-153-154.386c0-1.506,17.27-19.166,18.743-19.166c0.494,0,65.02,64.123,143.391,142.496
                      s142.905,142.496,143.409,142.496s65.038-64.123,143.409-142.496S-0.877,2127.395-0.114,2127.395S5.275,2131.41,10.164,2136.317
                      L10.164,2136.317z"/>
                  </g>
                  </svg>
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <?php echo get_field('dj_terminaciones',$page->ID);?>
              </div>
            </div>
          </div>
        </div>



        <a href="#modelos_j" class="btn-main-inv" type="button" name="button"><?php echo get_field('dj_texto_ver_plantas',$page->ID);?></a>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-7">
        <!-- Exclusivos Carusel -->
        <div class="owl-carousel owl-theme carousel-exclusivos">
            <?php

            foreach ($dj_galeria_imagenes as $key => $value) {
              $imagen = wp_get_attachment_image_src( $value ,'full');
              $large = wp_get_attachment_image_src( $value ,'large');
              $medium = wp_get_attachment_image_src( $value ,'medium');
              $thumbnail = wp_get_attachment_image_src( $value ,'thumbnail');
              ?>

            <div class="item">
              <div class="thumbnail">

                <picture>
                  <source media="(max-width: 768px)" srcset="<?php echo $medium[0];?>">
                  <source media="(max-width: 1024px)" srcset="<?php echo $large[0];?>">
                  <img src="<?php echo $imagen[0];?>" width="1500" height="400" alt="Hero">
                </picture>

              </div>
            </div>

              <?php
            }
            ?>

        </div>
      </div>
    </div>

  </div>
</div>



    <div id="target_69" class="container-fluid">

    <?php include('module_vista_modelo.php');?>

      <!-- MAIN CAROUSEL JARDIN-->
      <div id="modelos_j" class="row seccion modelos jardines gray-back">
        <img class="depto-diseno" src="<?php echo get_template_directory_uri();?>/img/DEPTO-DISENO.png" alt="">
        <div class="col-sm-12">
          <h1 class="purple"><?php echo get_field('dj_titulo_modelos',$page->ID);?></h1> <h2 class="purple"><?php echo get_field('dj_subtitulo_modelos',$page->ID);?></h2>
          <div class="owl-carousel owl-theme carousel-modelos">

            <?php
              foreach ($deptos  as $key => $value) {

                ?>

            <div class="item">
              <div class="thumbnail">
                <h3 class="text-center"><?php echo get_field('titulo',$value->ID);?></h3>

                <img src="<?php echo get_field('imagen_home',$value->ID);?>" alt="">



                <div class="caption">

                  <h4>Tipo <?php echo get_field('tipo',$value->ID);?></h4>
                  <p><?php echo get_field('texto_bajada',$value->ID);?></p>

                  <a href="#vista-modelos-jardin-ctn"><button class="btn-ver-inv" type="button" name="button"
                  data-clase="Jardín"
                  data-id="<?php echo $value->ID;?>"
                  data-imagen_home="<?php echo get_field('imagen_home',$value->ID);?>"
                  data-titulo="<?php echo get_field('titulo',$value->ID);?>"
                  data-tipo="<?php echo get_field('tipo',$value->ID);?>"
                  data-texto_bajada="<?php echo get_field('texto_bajada',$value->ID);?>"
                  data-dormitorios="<?php echo get_field('dormitorios',$value->ID);?>"
                  data-banos="<?php echo get_field('banos',$value->ID);?>"
                  data-cocina="<?php echo get_field('cocina',$value->ID);?>"
                  data-quincho_o_jardin="<?php echo get_field('quincho_o_jardin',$value->ID);?>"
                  data-superficie_util="<?php echo get_field('superficie_util',$value->ID);?>"
                  data-superficie_total="<?php echo get_field('superficie_total',$value->ID);?>"
                  data-superficie_quincho_o_jardin="<?php echo get_field('superficie_quincho_o_jardin',$value->ID);?>"
                  data-superficie_terraza="<?php echo get_field('superficie_terraza',$value->ID);?>"
                  data-precio_uf="<?php echo get_field('precio_uf',$value->ID);?>"
                  data-precio_pie_20="<?php echo get_field('precio_pie_20',$value->ID);?>"
                  data-precio_credito="<?php echo get_field('precio_credito',$value->ID);?>"
                  data-imagen_cotizacion="<?php echo get_field('imagen_cotizacion',$value->ID);?>"
                  onclick="$('#vista-modelos_<?php echo $value->ID; ?>').fadeIn();  backCotizar( <?php echo $value->ID;?> , 'jardin')">VER +</button></a>
                </div>
              </div>
            </div>

                <?php
              }
            ?>


          </div>
        </div>
      </div>
      <!-- <div class="mensaje-carousel">
        <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> <b>Deslizar</b> <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
      </div> -->
    </div><!-- End-ModelosJardin -->
