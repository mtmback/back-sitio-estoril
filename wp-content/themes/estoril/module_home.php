    <?php
    $page = get_post(6);
    $galeria = get_field('slider_home',$page->ID);
    $galeria_imagenes = explode(",", $galeria);
    ?>

    <div class="container-fluid">
      <div id="target_<?php echo $page->ID?>" class="row seccion home">
        <div class="col-xs-12 col-sm-12 col-md-8 img-home hidden-xs hidden-sm">
          <!-- Exclusivos Carusel -->
          <div class="owl-carousel owl-theme carousel-exclusivos">

            <?php
              $value = get_field('depthouse_slider_imagen');

              $imagen = wp_get_attachment_image_src( $value ,'full');
              $large = wp_get_attachment_image_src( $value ,'large');
              $medium = wp_get_attachment_image_src( $value ,'medium');
              $thumbnail = wp_get_attachment_image_src( $value ,'thumbnail');
              ?>

            <div class="item">
              <div class="thumbnail">
                <picture>
                  <source media="(max-width: 425px)" srcset="<?php echo $thumbnail[0];?>">
                  <source media="(max-width: 768px)" srcset="<?php echo $medium[0];?>">
                  <source media="(max-width: 1024px)" srcset="<?php echo $large[0];?>">
                  <img class="imagen-carousel" src="<?php echo $imagen[0];?>" alt="Hero">
                </picture>

                <a href="#target_8">
                  <svg class="chevron-down" version="1.1"
                  	 id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                  	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="30px"
                  	 viewBox="0 162.5 612 399" enable-background="new 0 162.5 612 399" xml:space="preserve">
                  <g transform="translate(592.94753,-1935.4889)">
                  	<path d="M10.164,2136.317c4.889,4.907,8.889,9.532,8.889,10.277c0,0.745-68.85,70.204-153,154.353l-153,152.998l-153-152.998
                  		c-84.15-84.149-153-153.623-153-154.386c0-1.506,17.27-19.166,18.743-19.166c0.494,0,65.02,64.123,143.391,142.496
                  		s142.905,142.496,143.409,142.496s65.038-64.123,143.409-142.496S-0.877,2127.395-0.114,2127.395S5.275,2131.41,10.164,2136.317
                  		L10.164,2136.317z"/>
                  </g>
                  </svg>
                </a>
              </div>
            </div>

            <?php
              $value = get_field('penthouse_slider_imagen');

              $imagen = wp_get_attachment_image_src( $value ,'full');
              $large = wp_get_attachment_image_src( $value ,'large');
              $medium = wp_get_attachment_image_src( $value ,'medium');
              $thumbnail = wp_get_attachment_image_src( $value ,'thumbnail');
              ?>

            <div class="item">
              <div class="thumbnail">
                <picture>
                  <source media="(max-width: 425px)" srcset="<?php echo $thumbnail[0];?>">
                  <source media="(max-width: 768px)" srcset="<?php echo $medium[0];?>">
                  <source media="(max-width: 1024px)" srcset="<?php echo $large[0];?>">
                  <img class="imagen-carousel" src="<?php echo $imagen[0];?>" alt="Hero">
                </picture>

                <a href="#target_8">
                  <svg class="chevron-down" version="1.1"
                  	 id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                  	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="30px"
                  	 viewBox="0 162.5 612 399" enable-background="new 0 162.5 612 399" xml:space="preserve">
                  <g transform="translate(592.94753,-1935.4889)">
                  	<path d="M10.164,2136.317c4.889,4.907,8.889,9.532,8.889,10.277c0,0.745-68.85,70.204-153,154.353l-153,152.998l-153-152.998
                  		c-84.15-84.149-153-153.623-153-154.386c0-1.506,17.27-19.166,18.743-19.166c0.494,0,65.02,64.123,143.391,142.496
                  		s142.905,142.496,143.409,142.496s65.038-64.123,143.409-142.496S-0.877,2127.395-0.114,2127.395S5.275,2131.41,10.164,2136.317
                  		L10.164,2136.317z"/>
                  </g>
                  </svg>
                </a>
              </div>
            </div>



          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 info-home">
          <!-- <img class="logo hidden-md hidden-lg" src="" alt="Estoril Logo"> -->

          <a href="#target_69">
            <div class="row zona-depthouse">

              <div class="col-xs-6 dept-mobile hidden-md hidden-lg" style="background:url('<?php echo get_field('dept_house_mobile',$page->ID); ?>') no-repeat" >

              </div>

              <div class="col-xs-6 col-sm-12 col-lg-12 dept">
                <img class="info140" src="<?php echo get_field('depthouse_imagen_derecha',$page->ID);?>" alt="DeptHouse">
                <svg version="1.1"
                	 id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px"
                	 viewBox="125.5 136.5 357 365" enable-background="new 125.5 136.5 357 365" xml:space="preserve">
                    <g transform="translate(0,-952.36218)">
                    	<g transform="translate(20.998031,959.86021)">
                    		<g transform="translate(-11.994093,1.470475)">
                    			<path d="M290.924,145.292V304.46H131.756v12.144h159.168v159.168h12.144V316.603h159.168V304.46H303.068V145.292H290.924z"/>
                    		</g>
                    	</g>
                    </g>
                </svg>
              </div>

            </div>
          </a>

          <div class="row">
            <div class="col-xs-12 franja purple-back">
              <h1><?php echo get_field('texto_franja_home',$page->ID);?></h1>
            </div>
          </div>

          <a href="#target_70"><div class="row zona-penthouse">

            <div class="col-xs-6 col-sm-12 col-lg-12 dept">
              <img class="info140" src="<?php echo get_field('penthouse_imagen_derecha',$page->ID);?>" alt="PentHouse">
              <svg version="1.1"
                 id="Layer_1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"
                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px"
                 viewBox="125.5 136.5 357 365" enable-background="new 125.5 136.5 357 365" xml:space="preserve">
                  <g transform="translate(0,-952.36218)">
                    <g transform="translate(20.998031,959.86021)">
                      <g transform="translate(-11.994093,1.470475)">
                        <path d="M290.924,145.292V304.46H131.756v12.144h159.168v159.168h12.144V316.603h159.168V304.46H303.068V145.292H290.924z"/>
                      </g>
                    </g>
                  </g>
              </svg>
            </div>

            <div class="col-xs-6 dept-mobile hidden-md hidden-lg" style="background:url('<?php echo get_field('pent_house_mobile',$page->ID); ?>') no-repeat">

            </div>
          </div></a>

        </div>
      </div>
    </div>

    <!-- MODAL NAVIDAD -->
    <?php
      $date_target_start = "20171129";
      $date_target_end = "20171231";
      $date_now  = date('Ymd');
      if ( $date_now > $date_target_start && $date_now < $date_target_end ){
          ?>
          <div id="modalNavidad" class="remodal" data-remodal-id="modal">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-izquierda">
                <img class="imagen-navidad" src="<?php echo get_template_directory_uri()?>/img/modalnav/christmas.svg" alt="">
              </div>
              <div class="col-xs-12 col-sm-6 col-derecha">
                <img class="imagen-oferta" src="<?php echo get_template_directory_uri()?>/img/modalnav/objeto2.svg" alt="">
                <a href="#target_70" class="remodal-confirm btn-popup" style="margin-top: 20px;" onclick="inst.close()">COTIZA HOY</a>
              </div>
            </div>
          </div>
          <?php
      }
      ?>
