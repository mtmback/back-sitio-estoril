function initMap() {
  initMapfull('map');
}

function initMapfull (id) {
  var map = new google.maps.Map(document.getElementById(id), {
    center: {lat: -33.423819, lng: -70.619821},
    zoom: 15,
    scrollwheel: false,
    styles:[
    {
        "featureType": "all",
        "elementType": "all",
        "stylers": [
            {
                "gamma": "0.83"
            },
            {
                "saturation": "-86"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "geometry",
        "stylers": [
            {
                "saturation": "-31"
            },
            {
                "lightness": "15"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "saturation": "-99"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels",
        "stylers": [
            {
                "saturation": "-63"
            },
            {
                "gamma": "1.14"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text",
        "stylers": [
            {
                "gamma": "0.92"
            },
            {
                "saturation": "-75"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "gamma": "1.29"
            },
            {
                "saturation": "-6"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "gamma": "1.00"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "weight": "0.01"
            },
            {
                "gamma": "0.00"
            },
            {
                "lightness": "-29"
            },
            {
                "saturation": "-82"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "all",
        "stylers": [
            {
                "gamma": "1.00"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-32"
            },
            {
                "lightness": "-8"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#54274e"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [
            {
                "saturation": "2"
            },
            {
                "lightness": "-13"
            },
            {
                "gamma": "8.46"
            },
            {
                "weight": "2.14"
            },
            {
                "invert_lightness": true
            }
        ]
    }
]
  });
  var marker = new google.maps.Marker({
    position: {lat: -33.423819, lng: -70.619821},
    map: map,
    icon: 'img/iconos-ubicacion/estoril-pinpoint.svg',
    title: 'Estoril'
    });
}
